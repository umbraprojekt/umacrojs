module.exports = function(config) {
	config.set({
		files: [
			{ pattern: "lib/**/*.js", mutated: true, included: false },
			{ pattern: "spec/resources/**/*", mutated: false, included: false },
			"spec/**/*.spec.js"
		],
		testRunner: "mocha",
		testFramework: "mocha",
		coverageAnalysis: "perTest",
		reporter: ["clear-text", "progress"]
	});
};
