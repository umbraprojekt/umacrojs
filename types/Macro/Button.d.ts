import { AbstractMacro } from "../AbstractMacro";
import { Params } from "../Params";
export declare class Button extends AbstractMacro {
    protected config(): void;
    produceOutput(params: Params, body?: string): string;
}
