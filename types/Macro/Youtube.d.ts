import { AbstractMacro } from "../AbstractMacro";
import { Params } from "../Params";
export declare class Youtube extends AbstractMacro {
    protected config(): void;
    produceOutput(params: Params, body?: string): string;
}
