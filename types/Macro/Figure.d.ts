import { AbstractMacro } from "../AbstractMacro";
import { Params } from "../Params";
export declare class Figure extends AbstractMacro {
    protected config(): void;
    produceOutput(params: Params, body?: string): string;
}
