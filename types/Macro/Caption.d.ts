import { AbstractMacro } from "../AbstractMacro";
import { Params } from "../Params";
export declare class Caption extends AbstractMacro {
    produceOutput(params: Params, body?: string): string;
}
