import { ParamParser, ParamParserConstructor } from "./ParamParser";
export declare class ParamParserFactory {
    static make(parseFunction: (input: string) => {}): ParamParserConstructor;
    static makeInstance(parseFunction: (input: string) => {}): ParamParser;
}
