export interface Macro {
    run(params: any, body?: string): string;
}
export interface MacroConstructor {
    new (): Macro;
}
