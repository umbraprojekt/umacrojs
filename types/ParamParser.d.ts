export interface ParamParser {
    parse(input: string): {};
}
export interface ParamParserConstructor {
    new (): ParamParser;
}
