import { ParamParser } from "./ParamParser";
import { Macro } from "./Macro";
export interface ParserOptions {
    open?: string;
    separator?: string;
    close?: string;
    paramParser?: ParamParser;
}
export declare class Parser {
    private _openMacroRegex;
    private _macroRegex;
    private _paramParser;
    private _macros;
    constructor(options?: ParserOptions);
    parse(input: string): string;
    addMacro(name: string, macro: Macro): void;
}
