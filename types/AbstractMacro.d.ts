import { Macro } from "./Macro";
import { Params } from "./Params";
export declare abstract class AbstractMacro implements Macro {
    private _paramDictionary;
    private _params;
    constructor();
    protected config(): void;
    protected abstract produceOutput(params: Params, body?: string): string;
    run(params: any, body?: string): string;
    addParam(name: string, defaultValue?: string): void;
    addRequiredParam(name: string): void;
    setParams(params: Params): void;
    verifyParams(): void;
    getParam(name: string): string;
    getDefaultHTMLAttributesString(params: Params): string;
}
