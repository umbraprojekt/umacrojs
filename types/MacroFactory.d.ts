import { Macro, MacroConstructor } from "./Macro";
export interface ParamConfig {
    required?: boolean;
    defaultValue?: string;
}
export interface ParamsConfig {
    [index: string]: ParamConfig;
}
export declare class MacroFactory {
    static make(paramsConfig: ParamsConfig, outputFunction: (params: any, body?: string) => string): MacroConstructor;
    static makeInstance(paramsConfig: ParamsConfig, outputFunction: (params: any) => string): Macro;
}
