const expect = require("chai").expect;
const MacroFactory = require("../lib/MacroFactory").MacroFactory;
const AbstractMacro = require("../lib/AbstractMacro").AbstractMacro;

describe("MacroFactory", () => {
	let macro;
	const paramsConfig = {
		id: {required: true},
		a: {required: false, defaultValue: "foo"},
		b: {defaultValue: "bar"},
		c: {required: false},
		d: {}
	};
	const outputFunction = (params) => {
		return JSON.stringify(params);
	};

	it("should create macro instances", () => {
		// when
		macro = MacroFactory.makeInstance(paramsConfig, outputFunction);

		// then
		expect(macro).to.be.an.instanceOf(AbstractMacro);
	});

	it("should create macro constructors", () => {
		// given
		const ctor = MacroFactory.make(paramsConfig, outputFunction);

		// when
		macro = new ctor();

		// then
		expect(macro).to.be.an.instanceOf(AbstractMacro);
	});

	describe("after creating a macro", () => {
		beforeEach(() => {
			macro = MacroFactory.makeInstance(paramsConfig, outputFunction);
		});

		it("should produce correct output with minimal param set", () => {
			// when
			const output = macro.run({id: "ID"});

			// then
			expect(output).to.equal('{"id":"ID","a":"foo","b":"bar"}');
		});

		it("should produce correct output with full param set", () => {
			// when
			const output = macro.run({
				id: "ID",
				a: "A",
				b: "B",
				c: "C",
				d: "D"
			});

			// then
			expect(output).to.equal('{"id":"ID","a":"A","b":"B","c":"C","d":"D"}');
		});

		it("should throw on missing params", () => {
			const fn = () => {
				macro.run({});
			};

			expect(fn).to.throw('Parametre "id" is required.');
		});
	});
});
