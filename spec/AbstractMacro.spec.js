const expect = require("chai").expect;
const AbstractMacro = require("../lib/AbstractMacro").AbstractMacro;

describe("AbstractMacro", () => {
	it("should create a default HTML attributes string", () => {
		// given
		const macro = new AbstractMacro();
		const params = {
			id: "foo",
			"aria-hidden": "bar",
			"data-qux": "qux"
		};

		// when
		const result = macro.getDefaultHTMLAttributesString(params);

		// then
		expect(result).to.equal(' id="foo" aria-hidden="bar" data-qux="qux"');
	});
});
