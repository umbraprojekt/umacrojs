const expect = require("chai").expect;
const fs = require("fs");
const Parser = require("../lib/Parser").Parser;
const QueryString = require("../lib/ParamParser/QueryString").QueryString;

const read = (filename) => {
	return fs.readFileSync(`${__dirname}/${filename}`).toString();
};

describe("Parser", () => {
	it("should parse strings with default settings", () => {
		// given
		const parser = new Parser();
		const input = read("resources/parser/default/input.html");
		const expectedOutput = read("resources/parser/default/output.html");

		// when
		const output = parser.parse(input);

		// then
		expect(output).to.equal(expectedOutput);
	});

	it("should parse strings with custom settings", () => {
		// given
		const parser = new Parser({
			open: "{{{",
			close: "}}}",
			separator: "|"
		});
		const input = read("resources/parser/custom/input.html");
		const expectedOutput = read("resources/parser/custom/output.html");

		// when
		const output = parser.parse(input);

		// then
		expect(output).to.equal(expectedOutput);
	});

	it("should parse strings with non-default parser", () => {
		// given
		const parser = new Parser({
			paramParser: new QueryString(),
			separator: "?"
		});
		const input = read("resources/parser/querystring/input.html");
		const expectedOutput = read("resources/parser/querystring/output.html");

		// when
		const output = parser.parse(input);

		// then
		expect(output).to.equal(expectedOutput);
	});

	it("should allow adding new parsers", () => {
		// given
		class EvalParser {
			parse(input) {
				return eval(`(${input})`);
			}
		}

		const parser = new Parser({
			paramParser: new EvalParser()
		});
		const input = read("resources/parser/eval/input.html");
		const expectedOutput = read("resources/parser/eval/output.html");

		// when
		const output = parser.parse(input);

		// then
		expect(output).to.equal(expectedOutput);
	});

	it("should allow adding new macros", () => {
		// given
		class DummyMacro {
			run(params) {
				const stringifiedParams = JSON.stringify(params);
				return `<dummy>${stringifiedParams}</dummy>`;
			}
		}


		const parser = new Parser();
		parser.addMacro("dummy", new DummyMacro());

		const input = read("resources/parser/dummy/input.html");
		const expectedOutput = read("resources/parser/dummy/output.html");

		// when
		const output = parser.parse(input);

		// then
		expect(output).to.equal(expectedOutput);
	});

	it("should work for enclosing macros", () => {
		// given
		const parser = new Parser();
		const input = read("resources/parser/enclosing/input.html");
		const expectedOutput = read("resources/parser/enclosing/output.html");

		// when
		const output = parser.parse(input);

		// then
		expect(output).to.equal(expectedOutput);
	});

	it("should work for mixed and nested self-closing and enclosing macros", () => {
		// given
		const parser = new Parser();
		const input = read("resources/parser/mixed/input.html");
		const expectedOutput = read("resources/parser/mixed/output.html");

		// when
		const output = parser.parse(input);

		// then
		expect(output).to.equal(expectedOutput);
	});
});
