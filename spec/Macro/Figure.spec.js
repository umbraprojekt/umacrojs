const expect = require("chai").expect;
const Figure = require("../../lib/Macro/Figure").Figure;

describe("Figure macro", () => {
	let macro;

	beforeEach(() => {
		macro = new Figure();
	});

	it("should work with no params", () => {
		// given
		const body = '<img src="http://i.imgur.com/hZ3AlAn.jpg" />';

		// when
		const output = macro.run({}, body);

		// then
		expect(output).to.equal(`<figure>${body}</figure>`);
	});

	it("should work with params", () => {
		// given
		const body = '<img src="http://i.imgur.com/hZ3AlAn.jpg" />';
		const params = {
			id: "foo",
			class: "bar"
		};

		// when
		const output = macro.run(params, body);

		// then
		expect(output).to.equal(`<figure id="foo" class="bar">${body}</figure>`);
	});
});
