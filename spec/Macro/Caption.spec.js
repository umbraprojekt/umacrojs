const expect = require("chai").expect;
const Caption = require("../../lib/Macro/Caption").Caption;

describe("Caption macro", () => {
	let macro;

	beforeEach(() => {
		macro = new Caption();
	});

	it("should work with no params", () => {
		// given
		const body = '<cite>All your base are belong to us!</cite>';

		// when
		const output = macro.run({}, body);

		// then
		expect(output).to.equal(`<figcaption>${body}</figcaption>`);
	});
});
