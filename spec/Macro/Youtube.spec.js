const expect = require("chai").expect;
const Youtube = require("../../lib/Macro/Youtube").Youtube;

describe("Youtube macro", () => {
	let macro;

	beforeEach(() => {
		macro = new Youtube();
	});

	it("should work with all params", () => {
		// given
		const params = {
			id: "dQw4w9WgXcQ",
			width: "320",
			height: "240"
		};

		// when
		const output = macro.run(params);

		// then
		expect(output).to.equal('<iframe width="320" height="240" src="https://www.youtube.com/embed/dQw4w9WgXcQ" frameborder="0"></iframe>');
	});

	it("should work with default params", () => {
		// given
		const params = {
			id: "dQw4w9WgXcQ"
		};

		// when
		const output = macro.run(params);

		// then
		expect(output).to.equal('<iframe width="560" height="315" src="https://www.youtube.com/embed/dQw4w9WgXcQ" frameborder="0"></iframe>');
	});

	it("should throw on missing params", () => {
		const params = {};
		const fn = () => {
			macro.run(params);
		};

		expect(fn).to.throw();
	});
});
