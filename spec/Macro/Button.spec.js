const expect = require("chai").expect;
const Button = require("../../lib/Macro/Button").Button;

describe("Button macro", () => {
	let macro;

	beforeEach(() => {
		macro = new Button();
	});

	it("should work with no params", () => {
		// given
		const body = "Clicky!";

		// when
		const output = macro.run({}, body);

		// then
		expect(output).to.equal(`<button>${body}</button>`);
	});

	it("should work with params", () => {
		// given
		const body = "Clicky!";
		const params = {
			id: "foo",
			class: "bar"
		};

		// when
		const output = macro.run(params, body);

		// then
		expect(output).to.equal(`<button id="foo" class="bar">${body}</button>`);
	});
});
