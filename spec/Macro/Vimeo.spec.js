const expect = require("chai").expect;
const Vimeo = require("../../lib/Macro/Vimeo").Vimeo;

describe("Vimeo macro", () => {
	let macro;

	beforeEach(() => {
		macro = new Vimeo();
	});

	it("should work with all params", () => {
		// given
		const params = {
			id: "71336599",
			width: "320",
			height: "240"
		};

		// when
		const output = macro.run(params);

		// then
		expect(output).to.equal('<iframe width="320" height="240" src="http://player.vimeo.com/video/71336599" frameborder="0"></iframe>');
	});

	it("should work with default params", () => {
		// given
		const params = {
			id: "71336599"
		};

		// when
		const output = macro.run(params);

		// then
		expect(output).to.equal('<iframe width="560" height="315" src="http://player.vimeo.com/video/71336599" frameborder="0"></iframe>');
	});

	it("should throw on missing params", () => {
		const params = {};
		const fn = () => {
			macro.run(params);
		};

		expect(fn).to.throw();
	});
});
