const expect = require("chai").expect;
const ParamParserFactory = require("../lib/ParamParserFactory").ParamParserFactory;

describe("ParamParserFactory", () => {
	let parser;
	const parseFunction = (input) => {
		return JSON.parse(input);
	};

	it("should create param parser instances", () => {
		// when
		parser = ParamParserFactory.makeInstance(parseFunction);

		// then
		expect(parser).to.be.an("object");
		expect(parser.parse).to.be.a("function");
	});

	it("should create param parser constructors", () => {
		// given
		const ctor = ParamParserFactory.make(parseFunction);

		// when
		parser = new ctor();

		// then
		expect(parser).to.be.an("object");
		expect(parser.parse).to.be.a("function");
	});

	describe("after creating a param parser", () => {
		beforeEach(() => {
			parser = ParamParserFactory.makeInstance(parseFunction);
		});

		it("should parse params according to specified logic", () => {
			// given
			const input = '{"foo":"bar","bat":"man"}';

			// when
			const output = parser.parse(input);

			// when
			expect(output).to.deep.equal({foo: "bar", bat: "man"});
		});
	});
});
