const expect = require("chai").expect;
const QueryString = require("../../lib/ParamParser/QueryString").QueryString;

describe("Querystring param parser", () => {
	it("should parse query strings", () => {
		// given
		const parser = new QueryString();

		// when
		const output = parser.parse("foo=bar&baz=qux&baz=quux&bat=man");

		// then
		expect(output).to.deep.equal({
			foo: "bar",
			baz: ["qux", "quux"],
			bat: "man"
		});
	});
});
