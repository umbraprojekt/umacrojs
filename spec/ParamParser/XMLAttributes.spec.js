const expect = require("chai").expect;
const XMLAttributes = require("../../lib/ParamParser/XMLAttributes").XMLAttributes;

describe("XMLAttributes param parser", () => {
	it("should parse XML attributes", () => {
		// given
		const parser = new XMLAttributes();

		// when
		const output = parser.parse('foo="bar" bat="man"  qux="quux"');

		// then
		expect(output).to.deep.equal({
			foo: "bar",
			bat: "man",
			qux: "quux"
		});
	});
});
