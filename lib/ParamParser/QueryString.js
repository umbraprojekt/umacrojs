"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var query_string_1 = require("query-string");
var QueryString = (function () {
    function QueryString() {
    }
    QueryString.prototype.parse = function (input) {
        return !!input ? query_string_1.parse(input) : {};
    };
    return QueryString;
}());
exports.QueryString = QueryString;
