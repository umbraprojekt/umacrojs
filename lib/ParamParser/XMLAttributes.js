"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var XMLAttributes = (function () {
    function XMLAttributes() {
        this.attribRegex = new RegExp('([^=\\s]+)="([^"]*)"', "g");
    }
    XMLAttributes.prototype.parse = function (input) {
        var output = {};
        !!input && input.replace(this.attribRegex, function (match, key, value) {
            output[key] = value;
            return match;
        });
        return output;
    };
    return XMLAttributes;
}());
exports.XMLAttributes = XMLAttributes;
