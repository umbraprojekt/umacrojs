"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var AbstractMacro = (function () {
    function AbstractMacro() {
        this._paramDictionary = {};
        this.config();
    }
    AbstractMacro.prototype.config = function () { };
    ;
    AbstractMacro.prototype.run = function (params, body) {
        this.setParams(params);
        this.verifyParams();
        return this.produceOutput(this._params, body);
    };
    AbstractMacro.prototype.addParam = function (name, defaultValue) {
        this._paramDictionary[name] = {
            required: false
        };
        if (!!defaultValue || typeof defaultValue === "string") {
            this._paramDictionary[name].defaultValue = defaultValue;
        }
    };
    AbstractMacro.prototype.addRequiredParam = function (name) {
        this._paramDictionary[name] = {
            required: true
        };
    };
    AbstractMacro.prototype.setParams = function (params) {
        var _this = this;
        this._params = params;
        Object.keys(this._paramDictionary).forEach(function (paramName) {
            var param = _this._paramDictionary[paramName];
            if (_this._params[paramName] === undefined && param.defaultValue !== undefined) {
                _this._params[paramName] = param.defaultValue;
            }
        });
    };
    AbstractMacro.prototype.verifyParams = function () {
        var _this = this;
        Object.keys(this._paramDictionary).forEach(function (paramName) {
            var param = _this._paramDictionary[paramName];
            if (param.required && _this._params[paramName] === undefined) {
                throw new Error("Parametre \"" + paramName + "\" is required.");
            }
        });
    };
    AbstractMacro.prototype.getParam = function (name) {
        return this._params[name] ?
            this._params[name] :
            (this._paramDictionary[name] ?
                this._paramDictionary[name].defaultValue :
                undefined);
    };
    AbstractMacro.prototype.getDefaultHTMLAttributesString = function (params) {
        var defaultAttributes = [
            "accesskey", "class", "contenteditable", "contextmenu", "dir", "draggable", "dropzone", "hidden", "id",
            "onabort", "onautocomplete", "onautocompleteerror", "onblur", "oncancel", "oncanplay", "oncanplaythrough",
            "onchange", "onclick", "onclose", "oncontextmenu", "oncuechange", "ondblclick", "ondrag", "ondragend",
            "ondragenter", "ondragexit", "ondragleave", "ondragover", "ondragstart", "ondrop", "ondurationchange",
            "onemptied", "onended", "onerror", "onfocus", "oninput", "oninvalid", "onkeydown", "onkeypress", "onkeyup",
            "onload", "onloadeddata", "onloadedmetadata", "onloadstart", "onmousedown", "onmouseenter", "onmouseleave",
            "onmousemove", "onmouseout", "onmouseover", "onmouseup", "onmousewheel", "onpause", "onplay", "onplaying",
            "onprogress", "onratechange", "onreset", "onresize", "onscroll", "onseeked", "onseeking", "onselect",
            "onshow", "onsort", "onstalled", "onsubmit", "onsuspend", "ontimeupdate", "ontoggle", "onvolumechange",
            "onwaiting"
        ];
        return Object.keys(params)
            .filter(function (key) { return defaultAttributes.indexOf(key) > -1 || key.match(/^(aria|data)-.*$/); })
            .map(function (key) { return " " + key + "=\"" + params[key] + "\""; })
            .join("");
    };
    return AbstractMacro;
}());
exports.AbstractMacro = AbstractMacro;
