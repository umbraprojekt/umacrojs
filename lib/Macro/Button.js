"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var AbstractMacro_1 = require("../AbstractMacro");
var Button = (function (_super) {
    __extends(Button, _super);
    function Button() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Button.prototype.config = function () {
        this.addParam("class");
        this.addParam("id");
    };
    Button.prototype.produceOutput = function (params, body) {
        return "<button"
            + (params.id ? " id=\"" + params.id + "\"" : "")
            + (params.class ? " class=\"" + params.class + "\"" : "")
            + (">" + body + "</button>");
    };
    return Button;
}(AbstractMacro_1.AbstractMacro));
exports.Button = Button;
