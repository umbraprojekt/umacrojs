"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var AbstractMacro_1 = require("../AbstractMacro");
var Caption = (function (_super) {
    __extends(Caption, _super);
    function Caption() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Caption.prototype.produceOutput = function (params, body) {
        return "<figcaption>" + body + "</figcaption>";
    };
    return Caption;
}(AbstractMacro_1.AbstractMacro));
exports.Caption = Caption;
