"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var AbstractMacro_1 = require("../AbstractMacro");
var Youtube = (function (_super) {
    __extends(Youtube, _super);
    function Youtube() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Youtube.prototype.config = function () {
        this.addRequiredParam("id");
        this.addParam("width", "560");
        this.addParam("height", "315");
    };
    Youtube.prototype.produceOutput = function (params, body) {
        return "<iframe width=\"" + params["width"] + "\" height=\"" + params["height"] + "\" " +
            ("src=\"https://www.youtube.com/embed/" + params["id"] + "\" frameborder=\"0\"></iframe>");
    };
    return Youtube;
}(AbstractMacro_1.AbstractMacro));
exports.Youtube = Youtube;
