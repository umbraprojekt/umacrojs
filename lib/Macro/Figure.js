"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var AbstractMacro_1 = require("../AbstractMacro");
var Figure = (function (_super) {
    __extends(Figure, _super);
    function Figure() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Figure.prototype.config = function () {
        this.addParam("class");
        this.addParam("id");
    };
    Figure.prototype.produceOutput = function (params, body) {
        return "<figure"
            + (params.id ? " id=\"" + params.id + "\"" : "")
            + (params.class ? " class=\"" + params.class + "\"" : "")
            + (">" + body + "</figure>");
    };
    return Figure;
}(AbstractMacro_1.AbstractMacro));
exports.Figure = Figure;
