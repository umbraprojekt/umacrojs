"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var ParamParserFactory = (function () {
    function ParamParserFactory() {
    }
    ParamParserFactory.make = function (parseFunction) {
        var CustomParamParser = (function () {
            function CustomParamParser() {
            }
            CustomParamParser.prototype.parse = function (input) {
                return !!input ? parseFunction(input) : {};
            };
            return CustomParamParser;
        }());
        return CustomParamParser;
    };
    ParamParserFactory.makeInstance = function (parseFunction) {
        return new (this.make(parseFunction))();
    };
    return ParamParserFactory;
}());
exports.ParamParserFactory = ParamParserFactory;
