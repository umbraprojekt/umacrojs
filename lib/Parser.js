"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Youtube_1 = require("./Macro/Youtube");
var Vimeo_1 = require("./Macro/Vimeo");
var Button_1 = require("./Macro/Button");
var XMLAttributes_1 = require("./ParamParser/XMLAttributes");
var quote = function (input) {
    return (input + "").replace(/[.?*+^$[\]\\(){}|-]/g, "\\$&");
};
var Parser = (function () {
    function Parser(options) {
        if (options === void 0) { options = {}; }
        var openMarker = quote(options.open ? options.open : "[");
        var separator = quote(options.separator ? options.separator : " ");
        var closeMarker = quote(options.close ? options.close : "]");
        var open = openMarker + "(\\w+)(" + separator + "(.+))?" + closeMarker;
        var body = "([\\s\\S]*?)";
        var close = openMarker + "\\/(\\w+)" + closeMarker;
        this._openMacroRegex = new RegExp(open, "g");
        this._macroRegex = new RegExp(open + body + close, "g");
        this._paramParser = options.paramParser ? options.paramParser : new XMLAttributes_1.XMLAttributes();
        this._macros = {
            youtube: new Youtube_1.Youtube(),
            vimeo: new Vimeo_1.Vimeo(),
            button: new Button_1.Button()
        };
    }
    Parser.prototype.parse = function (input) {
        var _this = this;
        var output = "";
        var indices = [];
        // get the offsets of all open macros
        input.replace(this._openMacroRegex, function (matchedString, macroName, rubbish, params, offset) {
            indices.push(offset);
            return matchedString;
        });
        // iterate over the offsets in reverse order
        indices.reverse().forEach(function (offset) {
            output = input.substr(offset) + output;
            input = input.substr(0, offset);
            output = output
                .replace(_this._macroRegex, function (matchedString, macroName, rubbish, params, body, repeatMacroName) {
                if (macroName !== repeatMacroName) {
                    return matchedString;
                }
                var macro = _this._macros[macroName];
                if (!macro) {
                    return matchedString;
                }
                var parsedParams = _this._paramParser.parse(params);
                return macro.run(parsedParams, body);
            })
                .replace(_this._openMacroRegex, function (matchedString, macroName, rubbish, params) {
                var macro = _this._macros[macroName];
                if (!macro) {
                    return matchedString;
                }
                var parsedParams = _this._paramParser.parse(params);
                return macro.run(parsedParams);
            });
        });
        return input + output;
    };
    Parser.prototype.addMacro = function (name, macro) {
        this._macros[name] = macro;
    };
    return Parser;
}());
exports.Parser = Parser;
