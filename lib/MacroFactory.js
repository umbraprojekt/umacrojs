"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var AbstractMacro_1 = require("./AbstractMacro");
var MacroFactory = (function () {
    function MacroFactory() {
    }
    MacroFactory.make = function (paramsConfig, outputFunction) {
        var CustomMacro = (function (_super) {
            __extends(CustomMacro, _super);
            function CustomMacro() {
                return _super !== null && _super.apply(this, arguments) || this;
            }
            CustomMacro.prototype.config = function () {
                var _this = this;
                Object.keys(paramsConfig).forEach(function (paramName) {
                    var param = paramsConfig[paramName];
                    if (param.required) {
                        _this.addRequiredParam(paramName);
                    }
                    else {
                        _this.addParam(paramName, param.defaultValue);
                    }
                });
            };
            CustomMacro.prototype.produceOutput = function (params, body) {
                return outputFunction(params, body);
            };
            return CustomMacro;
        }(AbstractMacro_1.AbstractMacro));
        return CustomMacro;
    };
    MacroFactory.makeInstance = function (paramsConfig, outputFunction) {
        return new (this.make(paramsConfig, outputFunction))();
    };
    return MacroFactory;
}());
exports.MacroFactory = MacroFactory;
