import {ParamParser} from "../ParamParser";
import {parse} from "query-string";

export class QueryString implements ParamParser {

	public parse(input: string): {} {
		return !!input ? parse(input) : {};
	}
}
