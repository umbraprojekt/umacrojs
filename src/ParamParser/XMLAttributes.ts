import {ParamParser} from "../ParamParser";

interface Output {
	[index: string]: string;
}

export class XMLAttributes implements ParamParser {

	private attribRegex = new RegExp('([^=\\s]+)="([^"]*)"', "g");

	public parse(input: string): {} {
		let output: Output = {};
		!!input && input.replace(this.attribRegex, (match, key, value) => {
			output[key] = value;
			return match;
		});
		return output;
	}
}
