import {ParamParser} from "./ParamParser";
import {Macro} from "./Macro";
import {Youtube} from "./Macro/Youtube";
import {Vimeo} from "./Macro/Vimeo";
import {Button} from "./Macro/Button";
import {XMLAttributes} from "./ParamParser/XMLAttributes";

let quote = function(input: string): string {
	return (input + "").replace(/[.?*+^$[\]\\(){}|-]/g, "\\$&");
};

interface MacroDictionary {
	[index: string]: Macro;
}

export interface ParserOptions {
	open?: string;
	separator?: string;
	close?: string;
	paramParser?: ParamParser;
}

export class Parser {

	private _openMacroRegex: RegExp;
	private _macroRegex: RegExp;
	private _paramParser: ParamParser;
	private _macros: MacroDictionary;

	constructor(options: ParserOptions = {}) {
		const openMarker = quote(options.open ? options.open : "[");
		const separator = quote(options.separator ? options.separator : " ");
		const closeMarker = quote(options.close ? options.close : "]");

		const open = `${openMarker}(\\w+)(${separator}(.+))?${closeMarker}`;
		const body = "([\\s\\S]*?)";
		const close = `${openMarker}\\/(\\w+)${closeMarker}`;

		this._openMacroRegex = new RegExp(open, "g");
		this._macroRegex = new RegExp(open + body + close, "g");
		this._paramParser = options.paramParser ? options.paramParser : new XMLAttributes();

		this._macros = {
			youtube: new Youtube(),
			vimeo: new Vimeo(),
			button: new Button()
		};
	}

	public parse(input: string): string {
		let output: string = "";
		const indices: Array<number> = [];

		// get the offsets of all open macros
		input.replace(this._openMacroRegex, (matchedString, macroName, rubbish, params, offset) => {
			indices.push(offset);
			return matchedString;
		});

		// iterate over the offsets in reverse order
		indices.reverse().forEach(offset => {
			output = input.substr(offset) + output;
			input = input.substr(0, offset);

			output = output
				// replace enclosing macros...
				.replace(this._macroRegex, (matchedString, macroName, rubbish, params, body, repeatMacroName) => {
					if (macroName !== repeatMacroName) {
						return matchedString;
					}

					const macro: Macro = this._macros[macroName];

					if (!macro) {
							return matchedString;
					}

					const parsedParams = this._paramParser.parse(params);

					return macro.run(parsedParams, body);
				})
				// replace self-closing macros
				.replace(this._openMacroRegex, (matchedString, macroName, rubbish, params) => {
					const macro: Macro = this._macros[macroName];

					if (!macro) {
						return matchedString;
					}

					const parsedParams = this._paramParser.parse(params);

					return macro.run(parsedParams);
				});
		});

		return input + output;
	}

	public addMacro(name: string, macro: Macro): void {
		this._macros[name] = macro;
	}
}
