import {AbstractMacro} from "./AbstractMacro";
import {Params} from "./Params";
import {Macro, MacroConstructor} from "./Macro";

export interface ParamConfig {
	required?: boolean;
	defaultValue?: string;
}
export interface ParamsConfig {
	[index: string]: ParamConfig;
}

export class MacroFactory {

	public static make(paramsConfig: ParamsConfig, outputFunction: (params: any, body?: string) => string): MacroConstructor {
		class CustomMacro extends AbstractMacro {
			protected config(): void {
				Object.keys(paramsConfig).forEach((paramName: string) => {
					let param = paramsConfig[paramName];
					if (param.required) {
						this.addRequiredParam(paramName);
					} else {
						this.addParam(paramName, param.defaultValue);
					}
				});
			}

			protected produceOutput(params: Params, body?: string): string {
				return outputFunction(params, body);
			}
		}

		return CustomMacro;
	}

	public static makeInstance(paramsConfig: ParamsConfig, outputFunction: (params: any) => string): Macro {
		return new (this.make(paramsConfig, outputFunction))();
	}
}
