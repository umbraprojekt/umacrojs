import {ParamParser, ParamParserConstructor} from "./ParamParser";

export class ParamParserFactory {

	public static make(parseFunction: (input: string) => {}): ParamParserConstructor {
		class CustomParamParser implements ParamParser {
			parse(input: string): {} {
				return !!input ? parseFunction(input) : {};
			}
		}
		return CustomParamParser;
	}

	public static makeInstance(parseFunction: (input: string) => {}): ParamParser {
		return new (this.make(parseFunction))();
	}
}
