import {Macro} from "./Macro";
import {Params} from "./Params";

interface ParamDefinition {
	defaultValue?: string;
	required: boolean;
}

interface ParamDictionary {
	[index: string]: ParamDefinition;
}

export abstract class AbstractMacro implements Macro {

	private _paramDictionary: ParamDictionary;
	private _params: Params;

	constructor() {
		this._paramDictionary = {};
		this.config();
	}

	protected config(): void {};

	protected abstract produceOutput(params: Params, body?: string): string;

	public run(params: any, body?: string): string {
		this.setParams(params);
		this.verifyParams();

		return this.produceOutput(this._params, body);
	}

	public addParam(name: string, defaultValue?: string): void {
		this._paramDictionary[name] = {
			required: false
		};
		if (!!defaultValue || typeof defaultValue === "string") {
			this._paramDictionary[name].defaultValue = defaultValue;
		}
	}

	public addRequiredParam(name: string): void {
		this._paramDictionary[name] = {
			required: true
		};
	}

	public setParams(params: Params): void {
		this._params = params;
		Object.keys(this._paramDictionary).forEach((paramName: string) => {
			let param = this._paramDictionary[paramName];
			if (this._params[paramName] === undefined && param.defaultValue !== undefined) {
				this._params[paramName] = param.defaultValue;
			}
		});
	}

	public verifyParams(): void {
		Object.keys(this._paramDictionary).forEach((paramName: string) => {
			let param = this._paramDictionary[paramName];
			if (param.required && this._params[paramName] === undefined) {
				throw new Error(`Parametre "${paramName}" is required.`);
			}
		});
	}

	public getParam(name: string): string {
		return this._params[name] ?
			this._params[name] :
			(this._paramDictionary[name] ?
				this._paramDictionary[name].defaultValue :
				undefined);
	}

	public getDefaultHTMLAttributesString(params: Params): string {
		const defaultAttributes: Array<string> = [
			"accesskey", "class", "contenteditable", "contextmenu", "dir", "draggable", "dropzone", "hidden", "id",
			"onabort", "onautocomplete", "onautocompleteerror", "onblur", "oncancel", "oncanplay", "oncanplaythrough",
			"onchange", "onclick", "onclose", "oncontextmenu", "oncuechange", "ondblclick", "ondrag", "ondragend",
			"ondragenter", "ondragexit", "ondragleave", "ondragover", "ondragstart", "ondrop", "ondurationchange",
			"onemptied", "onended", "onerror", "onfocus", "oninput", "oninvalid", "onkeydown", "onkeypress", "onkeyup",
			"onload", "onloadeddata", "onloadedmetadata", "onloadstart", "onmousedown", "onmouseenter", "onmouseleave",
			"onmousemove", "onmouseout", "onmouseover", "onmouseup", "onmousewheel", "onpause", "onplay", "onplaying",
			"onprogress", "onratechange", "onreset", "onresize", "onscroll", "onseeked", "onseeking", "onselect",
			"onshow", "onsort", "onstalled", "onsubmit", "onsuspend", "ontimeupdate", "ontoggle", "onvolumechange",
			"onwaiting"
		];

		return Object.keys(params)
			.filter(key => defaultAttributes.indexOf(key) > -1 || key.match(/^(aria|data)-.*$/))
			.map(key => ` ${key}="${params[key]}"`)
			.join("");
	}
}
