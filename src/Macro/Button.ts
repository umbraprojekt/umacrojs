import {AbstractMacro} from "../AbstractMacro";
import {Params} from "../Params";

export class Button extends AbstractMacro {

	protected config(): void {
		this.addParam("class");
		this.addParam("id");
	}

	public produceOutput(params: Params, body?: string): string {
		return "<button"
			+ (params.id ? ` id="${params.id}"` : "")
			+ (params.class ? ` class="${params.class}"` : "")
			+ `>${body}</button>`;
	}
}
