import {AbstractMacro} from "../AbstractMacro";
import {Params} from "../Params";

export class Caption extends AbstractMacro {

	public produceOutput(params: Params, body?: string): string {
		return `<figcaption>${body}</figcaption>`;
	}
}
