import {AbstractMacro} from "../AbstractMacro";
import {Params} from "../Params";

export class Youtube extends AbstractMacro {

	protected config(): void {
		this.addRequiredParam("id");
		this.addParam("width", "560");
		this.addParam("height", "315");
	}

	public produceOutput(params: Params, body?: string): string {
		return `<iframe width="${params["width"]}" height="${params["height"]}" ` +
			`src="https://www.youtube.com/embed/${params["id"]}" frameborder="0"></iframe>`;
	}
}
