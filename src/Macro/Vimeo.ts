import {AbstractMacro} from "../AbstractMacro";
import {Params} from "../Params";

export class Vimeo extends AbstractMacro {

	protected config(): void {
		this.addRequiredParam("id");
		this.addParam("width", "560");
		this.addParam("height", "315");
	}

	public produceOutput(params: Params, body?: string): string {
		return `<iframe width="${params["width"]}" height="${params["height"]}" ` +
			`src="http://player.vimeo.com/video/${params["id"]}" frameborder="0"></iframe>`;
	}
}
